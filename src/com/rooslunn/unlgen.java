package com.rooslunn;

import java.util.*;

public class unlgen {
    public static final int BALLS = 6;
    public static final int MIN_BALL = 1;
    public static final int MAX_BALL = 52 + MIN_BALL;

    public static void main(String[] args) {
        List<Integer> balls = new ArrayList<>(BALLS);
        Random rand = new Random();
        int roundsCount = roundsCount(args);

        for (int i = 0; i < roundsCount; i++) {
            System.out.printf(">>> Round %d\n", i + 1);
            for (int l = 0; l < BALLS; l++) {
                int nextNumber = randomBall(rand);
                while (balls.contains(nextNumber)) {
                    nextNumber = randomBall(rand);
                }
                balls.add(nextNumber);
            }
            Collections.sort(balls);
            System.out.println(balls);
            balls.clear();
        }

    }

    private static int randomBall(Random random) {
        return random.nextInt(MAX_BALL - MIN_BALL) + MIN_BALL;
    }

    private static int roundsCount(String[] args) {
        int result;

        if (args.length == 0) {
            return 1;
        }

        try {
            result = Integer.parseInt(args[0]);
            return result;
        } catch (NumberFormatException e) {
            System.out.printf("Argument %s must be an Integer\n", args[0]);
            System.exit(-1);
        }

        return 1;
    }
}
